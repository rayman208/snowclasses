﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowClasess
{
    class SnowFlake
    {
        private int x, y;
        private ConsoleColor color;
        private int speed;
        private char look;

        private int stepFall;

        public SnowFlake()
        {
            x = y = 0;
            color = ConsoleColor.White;
            speed = 1;
            look = '*';

            stepFall = 0;
        }

        public SnowFlake(int x, int y, ConsoleColor color, int speed, char look, int stepFall)
        {
            this.x = x;
            this.y = y;
            this.color = color;
            this.speed = speed;
            this.look = look;
            this.stepFall = stepFall;
        }

        public void Draw()
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = color;
            Console.Write(look);
        }

        public void Fall()
        {
            y += speed;

            stepFall++;
            if (stepFall % 2 == 0)
            {
                x++;
            }
            else
            {
                x--;
            }

            if (stepFall >= 10)
            {
                stepFall -= 10;
            }
        }

        /*public void SetY(int y)
        {
            this.y = y;
        }

        public int GetY()
        {
            return y;
        }*/

        public int Y
        {
            set { y = value; }
            get { return y; }
        }
    }
}
