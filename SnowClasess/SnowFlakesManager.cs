﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SnowClasess
{
    class SnowFlakesManager
    {
        private Random rnd;
        private int minX, minY, maxX, maxY;
        private SnowFlake[] snowFlakes;

        public SnowFlakesManager(int minX, int maxX, int minY, int maxY, int countSnowFlakes)
        {
            this.rnd = new Random();
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
            this.snowFlakes = new SnowFlake[countSnowFlakes];

            InitSnowFlakes();
        }

        private void InitSnowFlakes()
        {
            char[] looks = new[] { '&', '*', 'a', '@', '%', '$' };

            for (int i = 0; i < snowFlakes.Length; i++)
            {
                /*snowFlakes[i] = new SnowFlake(
                    rnd.Next(minX, maxX + 1),
                    rnd.Next(minY, maxY + 1),
                    (ConsoleColor)rnd.Next(1, 15 + 1),
                    rnd.Next(1, 3),
                    looks[rnd.Next(0, looks.Length)]
                    );*/
                snowFlakes[i] = new SnowFlake(
                    rnd.Next(minX, maxX + 1),
                    rnd.Next(minY, maxY + 1),
                    ConsoleColor.White,
                    rnd.Next(1,3),
                    '*',
                    rnd.Next(0,1+1)
                );
            }
        }

        private void MoveSnowFlakes()
        {
            for (int i = 0; i < snowFlakes.Length; i++)
            {
                snowFlakes[i].Fall();

                /*if (snowFlakes[i].GetY() > maxY)
                {
                    snowFlakes[i].SetY(minY);
                }*/

                if (snowFlakes[i].Y > maxY)
                {
                    snowFlakes[i].Y = minY;
                }
            }
        }

        private void DrawSnowFlakes()
        {
            for (int i = 0; i < snowFlakes.Length; i++)
            {
                snowFlakes[i].Draw();
            }
        }

        public void Run()
        {
            while (true)
            {
                Console.Clear();
                
                DrawSnowFlakes();

                MoveSnowFlakes();

                Thread.Sleep(400);
            }
        }
    }
}
