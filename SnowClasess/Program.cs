﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowClasess
{
    class Program
    {
        static void Main(string[] args)
        {
            SnowFlakesManager manager = new SnowFlakesManager(
                    3, Console.WindowWidth - 3,//X
                    0, Console.WindowHeight - 1,//Y
                    50);//COUNT

            manager.Run();
            

            Console.ReadKey();
        }
    }
}
